import { GET_TAG_LOADING, GET_TAG_SUCCESS, GET_TAG_ERROR, SENDING_SUCCESS, SENDING_ERROR } from './types';
import { getRootUrl } from '../Config';
import axios from 'axios';

export const getTagsFromResurce = (codiceRisorsaTecnologica) => dispatch => {
    axios.get(`${getRootUrl()}/visualizeTag/${codiceRisorsaTecnologica}`)
    .then(
        response => {
            dispatch({type:GET_TAG_SUCCESS, payload:response.data})
        }
    )
    .catch(
        error => {
            dispatch({type:GET_TAG_ERROR, payload:error})
        }
    )
    dispatch({type:GET_TAG_LOADING})
};

export const setTimeFromResurseButton = (codiceRisorsaTecnologica) => dispatch => { 
    axios.post(`${getRootUrl()}/updateTag/${codiceRisorsaTecnologica}`)
    .then(
        response => {
            dispatch({type:SENDING_SUCCESS, payload:response.data})
        }
    )
    .catch(error => dispatch({type:SENDING_ERROR, payload:error}))
};