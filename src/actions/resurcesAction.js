import { GET_RESURCES_LOADING, GET_RESURCES_SUCCESS, GET_RESURCES_ERROR} from './types';
import { getRootUrl } from '../Config';
import axios from 'axios';

export const getResurces = () => dispatch => {
    axios.get(`${getRootUrl()}/visualizeTag`)
    .then(
        response => {
            dispatch({type:GET_RESURCES_SUCCESS, payload:response.data})
        }
    )
    .catch(
        error => {
            dispatch({type:GET_RESURCES_ERROR, payload:error})
        }
    )
    dispatch({type:GET_RESURCES_LOADING})
};