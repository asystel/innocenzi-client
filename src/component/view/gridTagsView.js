import React, { Component } from 'react';
import { connect } from "react-redux";
import { Header, Grid, List } from 'semantic-ui-react';

export class GridTagsView extends Component {
    constructor(props){
        super(props);
        this.resurcesTagsPassed = null;
        this.state = {
            name:null,
            value:null,
            unit:null
        };       
    };
   
    componentWillMount(){
        this.resurcesTagsPassed =  this.resurcesTagsPassed;
        this.render();
    };

    render(){ 
        const RenderRepoListItem = item => (
            <Grid celled padded key={item.nome_acquisizione}>
                <Grid.Row>
                    <Grid.Column width={10} textAlign="left" verticalAlign="middle">
                        {item.nome_acquisizione} 
                    </Grid.Column>
                    <Grid.Column width={3} textAlign="center" verticalAlign="middle">
                        {item.ultimo_valore} 
                    </Grid.Column>
                    <Grid.Column width={3} textAlign="center" verticalAlign="middle">
                        {item.unita_misura} 
                    </Grid.Column>
                </Grid.Row>                                                                    
            </Grid>
        );
        //console.log('this.props.resurcesTags: ', this.props.resurcesTags);
        //console.log('this.state.loading: ', this.props.loading);
        if (this.props.resurcesTags){
            return(<List size="large">
                        <List.Item>
                            <Grid celled padded>
                                <Grid.Row color="teal">
                                    <Grid.Column width={10} textAlign="center" verticalAlign="middle">
                                        NOME ACQUISIZIONE
                                    </Grid.Column>
                                    <Grid.Column width={3} textAlign="center" verticalAlign="middle">
                                        ULTIMO VALORE
                                    </Grid.Column>
                                    <Grid.Column width={3} textAlign="center" verticalAlign="middle">
                                        UNITÀ DI MISURA
                                    </Grid.Column>
                                </Grid.Row>                                                                    
                            </Grid>  
                            {this.props.resurcesTags.map((item, index) => RenderRepoListItem(item, index))}
                        </List.Item> 
                    </List>                       
            );
        }else{
            return(
                <Header>
                    NESSUN DATO DA MOSTRARE
                </Header>
            );
        };
    };
}

export const mapStateToProps = function(state){
    const { data,  error, loading  } = state.tagView;   
    return { data, error, loading};
}

export default connect(mapStateToProps)(GridTagsView);