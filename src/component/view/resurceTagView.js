import React, { Component } from 'react';
import { connect } from "react-redux";
import { Container, Header, Button, Loader } from 'semantic-ui-react';
import { getTagsFromResurce, setTimeFromResurseButton } from '../../actions/tagViewAction';
import { Redirect } from 'react-router-dom';
import { GridTagsView } from '../view/gridTagsView';
import { getTimerRefresh } from '../../Config';
//import moment from 'moment';

var dateFormat = require('dateformat');

class resurceTagView extends Component {
    constructor(props){
        super(props);
        this.risorsaTecnologica='';
        this.refreshData = this.refreshData.bind(this);
        this.state = {
            timerON:null,
            resurce:null,
            dataSource:null,
            defaultState:null
        }       
    }

    refreshData(){
        this.props.getTagsFromResurce(this.risorsaTecnologica);
        this.setState({dataSource:this.props.data});
    }
    
    setTimer(){
        this.timerRefresh = setInterval(this.refreshData, getTimerRefresh());
        this.setState({timerOn:true});
    }

    componentWillMount(){
        const { match: { params } } = this.props;
        console.log(this.props)
        if (this.props.location.search !== ''){
            this.setState({defaultState:this.props.location.search.slice(1)})
        };

        this.risorsaTecnologica = params.undefined;
        this.props.getTagsFromResurce(this.risorsaTecnologica);
        this.setState({resurce:this.risorsaTecnologica});
        if (!this.state.timerOn){
            this.setTimer();
        }
    }

    componentWillUnmount(){
        clearInterval(this.timerRefresh);
        this.setState({dataSource:null});
        this.risorsaTecnologica = null;
        this.setState({timerOn:false});
    }

    goBack = function(){
        this.setState({resurce:null});
    } 

    saveDateTime = function(){
       this.props.setTimeFromResurseButton(this.state.resurce);
       this.props.getTagsFromResurce(this.risorsaTecnologica);
    }
     
    calcolatetimepast = function(inizialDateTime){
        return(this.lastUpdate(inizialDateTime));
    }

    lastUpdate = function(updatedata) {   
        //funzione che riceve come parametro una stringa di data/ora del tipo "[dd/MM/yyyy-HH:mm:ss]", ne fa il parsing e calcola la differenza con la data/ora
        //attuale, restituendo in uscita ore, minuti, secondi di differenza tra le due.
        //Autore: Alessandro Grechi
        //Data: 02/04/2013
        //Versione: 1.0
        
         
        //ottieni ora attuale
        var myDate = new Date();
        var year = myDate.getFullYear();
        var month = myDate.getMonth();
        if(month <= 9)
            month = '0'+(month+1);
        var day= myDate.getDate();
        if(day <= 9)
            day = '0'+day;
        var hour= myDate.getHours();
        if(hour<= 9)
            hour = '0'+hour;
        var minutes= myDate.getMinutes();
        if(minutes<= 9)
            minutes = '0'+minutes;
        var seconds= myDate.getSeconds();
        if(seconds<= 9)
            seconds = '0'+seconds;
         
         
        //Parsing della data/ora in ingresso [dd/MM/yyyy-HH:mm:ss]
         
        var str=updatedata;
        var n=str.slice(1,20);

        n=n.split("-");
        var daysplit=n[0].split("/");
        var datesplit=n[1].split(":");
         
        var day1=daysplit[0];
        var month1=daysplit[1];
        var year1=daysplit[2];
         
         
        var hour1=datesplit[0];
        var minutes1=datesplit[1];
        var seconds1=datesplit[2];
         
        //calcola la differenza
         
        var date1 = new Date(year1, month1, day1,  hour1, minutes1, seconds1); // ora in ingresso
        var date2 = new Date(year, month, day, hour, minutes, seconds); // ora attuale
        if (date2 < date1) {
            date2.setDate(date2.getDate() + 1);
        }
        var diff = date2 - date1;
         
        //converte i millisecondi in hh, mm, ss leggibili
        var msec = diff;
        var hh = Math.floor(msec / 1000 / 60 / 60);
        msec -= hh * 1000 * 60 * 60;
        var mm = Math.floor(msec / 1000 / 60);
        msec -= mm * 1000 * 60;
        var ss = Math.floor(msec / 1000);
        msec -= ss * 1000;
         
        //scrive il risultato della differenza (ultima misura: 0h, 0m, 0s fa)
        var result ="";
        //result += "Ultima misura: ";
        if(hh !== 0) {
            result += hh;
            if(hh===1){
                result += " ORA, ";
            }else{
                result += " ORE, ";
            };
        };
             
        if(mm !== 0) {
            result +=  mm;
            if(mm===1){
                result +=  " MINUTO E ";
            }else{
                result +=  " MINUTI E ";
            };
        }
        result += ss;
        if(ss===1){
            result += " SECONDO";
        }else{
            result += " SECONDI";
        };

        return result;
    }
    

    DateTimeSaved(){
        if (this.props.data[0].data_ora_tasto !== null){
              return(
              <Header>
                {dateFormat(this.props.data[0].data_ora_tasto, " dd/mm/yyyy HH:MM:ss ")}
                <br/>
                SONO PASSATI {this.calcolatetimepast(dateFormat(this.props.data[0].data_ora_tasto, " dd/mm/yyyy-HH:MM:ss "))}                                 
                <br/>
              </Header>
          )
        }
    }
    render(){
        if (this.state.resurce === null){
            return (<Redirect to={{pathname: "/", state:{defaultState: this.state.defaultState}}}/>);
        };

        if (this.state.dataSource === null) {
            if (this.state.buttonTorna === true){
                return( <Container>
                            <Header as='h3' textAlign='center'>
                            <br/>
                                NOME RISORSA TECNOLOGICA {this.state.resurce}
                            <br/>                       
                            </Header>
                            <Loader active inline='centered' size="large">Loading</Loader>                        
                            <Button fluid size='huge' onClick={() => this.goBack()}>
                                TORNA
                            </Button>                        
                        </Container>
                )
            }else{
                return( <Container>
                        <Header as='h3' textAlign='center'>
                        <br/>
                            NOME RISORSA TECNOLOGICA {this.state.resurce}
                        <br/>                       
                        </Header>
                        <Loader active inline='centered' size="large">Loading</Loader>                        
                    </Container>
                )
            }
        };

        if (this.props.data){
            let { length } = this.props.data;
            if(length > 0){
                if(this.state.defaultState === this.state.resurce){
                    return (<Container>
                                <Header as='h2' textAlign='center'>
                                    <br/>
                                        NOME RISORSA TECNOLOGICA {this.state.resurce}
                                    <br/> 
                                    <Button 
                                        fluid 
                                        size='huge' 
                                        onClick={() => this.saveDateTime()} 
                                        style={{
                                            backgroundColor:"lime", 
                                            color:"black"}} 
                                    >
                                        SALVA DATA E ORA 
                                    </Button> 
                                    {this.DateTimeSaved()}                                                                                                                                  
                                </Header> 
                                <GridTagsView resurcesTags={this.state.dataSource} />                            
                                <Button fluid size='huge' onClick={() => this.goBack()}>
                                  TORNA
                                </Button>                        
                            </Container> 
                    )
                }else{
                    return(<Container>
                                <Header as='h3' textAlign='center'>
                                <br/>
                                    NOME RISORSA TECNOLOGICA {this.state.resurce}
                                <br/>   
                                <Button 
                                    disabled
                                    fluid 
                                    size='huge' 
                                    style={{
                                        backgroundColor:"light-gray", 
                                         color:"black"}} 
                                >
                                    SALVA DATA E ORA 
                                </Button>
                                {this.DateTimeSaved()}                        
                                </Header>  
                                <GridTagsView resurcesTags={this.state.dataSource} />   
                                <Button fluid size='huge' onClick={() => this.goBack()}>
                                  TORNA
                                </Button>                    
                            </Container>
                    )
                }
            }else{
                return (
                    <Container>                
                        <Header as='h3' textAlign='center'>
                            <br/>
                                NESSUNA RISORSA TECNOLOGICA SELEZIONATA
                            <br/>
                        </Header>                
                        <Button fluid size='huge' onClick={() => this.goBack()}>
                            TORNA
                        </Button>                        
                    </Container>
                );                    
            };
        }else{
            return (
                <Container>                
                    <Header as='h3' textAlign='center'>
                        <br/>
                            CARICAMENTO PAGINA ...
                        <br/>
                    </Header>    
                    <Button fluid size='huge' onClick={() => this.goBack()}>
                        TORNA
                    </Button>                        
                                    
                </Container>
            );
        };
    }    
}

export const mapStateToProps = function(state){
    const { data,  error, loading } = state.tagView;   
    return { data, error, loading};
}

export default connect(mapStateToProps,{getTagsFromResurce,setTimeFromResurseButton})(resurceTagView);