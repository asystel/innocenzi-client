import React, { Component } from 'react';
import { connect } from "react-redux";
import { Container, Header, List, Button } from 'semantic-ui-react';
import { getResurces } from './actions/resurcesAction';
import { Redirect } from 'react-router-dom';

class App extends Component {
    constructor(props){
        super(props);
        this.state = {
            resurce:null,
            defaultState: null
        }       
    }

    componentDidMount(){
        if(!this.state.defaultState && (typeof this.props.location.state!== 'undefined'))
            this.setState({defaultState: this.props.location.state.defaultState})
    }

    componentWillMount(){
        this.props.getResurces();
    }
       
    pressedSetStore = function(value){
        this.setState({resurce:value});
    }

    goBack = function(){
        this.setState({resurce:null});
    }

    render() {
        const RenderRepoListItem = item => 
          <List.Item key={item.codice_risorsa_tecnologica}>
            <List.Content>
                <Button 
                    key={item.codice_risorsa_tecnologica} 
                    fluid 
                    size='huge' 
                    onClick={() => this.pressedSetStore(item.codice_risorsa_tecnologica)}
                >
                    {item.codice_risorsa_tecnologica}
                </Button>
            </List.Content>
          </List.Item>

        if((typeof this.state.resurce !== 'undefined') && (this.state.resurce !== 'null') && (this.state.resurce !== null)){
            return(  
                <Container>
                    <Header as='h2' textAlign='center'>
                        <br/>
                            <Redirect to={{
                                pathname:('/resurceTagView/'+ this.state.resurce),
                                search:this.state.defaultState ? "?"+this.state.defaultState : "", 
                                myres:this.state.resurce,
                                state:{defaultState: this.state.defaultState}    
                            }} 
                            />
                        <br/>
                    </Header>
                    <Button fluid size='huge' onClick={() => this.goBack()} >
                        TORNA
                    </Button>                                
                </Container>
            );
        }else if(this.props.data){
            return( 
                <Container>                
                    <Header as='h2' textAlign='center'>
                        <br/>
                            SCEGLI LA RISORSA TECNOLOGICA
                        <br/>
                    </Header>
                    <List>
                        {this.props.data.map((item, index) => RenderRepoListItem(item, index))}
                    </List>                
                </Container>                
            )
        }else if(this.props.loading){
            return(           
                <Container>
                    <Header as='h2' textAlign='center'>
                        <br/>
                            NESSUNA RISORSA TECNOLOGICA DISPONIBILE
                        <br/>
                    </Header>             
                </Container>
            );  
        }else{
            return(
                <Container>
                    <Header as='h2' textAlign='center'>
                        <br/>
                            CARICAMENTO...
                        <br/>
                    </Header>                       
                </Container>
            );
        }             
    }
}

export const mapStateToProps = function(state){
    const { data,  error, loading  } = state.resurces;   
    return { data, error, loading};
}

export default connect(mapStateToProps,{getResurces})(App);
