import Cookies from 'universal-cookie';

const API_VERSION = '/v1';
const PROTOCOL = 'http';
let HOST = 'localhost';
let PORT = '3600';

const BASE_PATH = '/api';
const TIMER_REFRESH = 5000;
//const DEFAULT_HOST='192.168.17.230';
//const DEFAULT_PORT='3000';

export function getRootUrl(){    
  return `${PROTOCOL}://${HOST}:${PORT}${BASE_PATH}${API_VERSION}`;
}

export function getTimerRefresh(){
  return `${TIMER_REFRESH}`;
}

export function getHostUrl(){    
  return `${HOST}`;
}

export function getPortUrl(){    
  return `${PORT}`;
}

export function setStore(value){
  const cookies = new Cookies();
  cookies.set('myresurce', value, { path: '/' });
  console.log('DATO SALVATO IN STORE: ', cookies.get('myresurce'));
  return `${cookies.get('myresurce')}`
}

export function getStore(){
  const cookies = new Cookies();
  let mycookie = cookies.get('myresurce');
  console.log('DATO LETTO IN STORE: ', mycookie);
  return `${mycookie}`
}


