import React from 'react';
import ReactDOM from 'react-dom';
import 'semantic-ui-css/semantic.min.css';
import registerServiceWorker from './registerServiceWorker';
import Root from './Router';
import store from './Store';
import {Provider} from 'react-redux';

ReactDOM.render(<Provider store={store}><Root /></Provider>, document.getElementById('root'));
registerServiceWorker();
