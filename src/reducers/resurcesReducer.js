import { GET_RESURCES_LOADING, GET_RESURCES_SUCCESS, GET_RESURCES_ERROR } from '../actions/types';

const INITIAL_STATE = {
    data:null,
    error:null,
    loading:false
};

export default function(state = INITIAL_STATE,action){
    switch (action.type){
        case GET_RESURCES_SUCCESS:
            return {...INITIAL_STATE,data:action.payload}
        case GET_RESURCES_ERROR:
            return {...INITIAL_STATE,error:'errore'}
        case GET_RESURCES_LOADING:
            return {...INITIAL_STATE,data:state.data,loading:true}        
        default:
            return state
    }
}