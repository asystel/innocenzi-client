import {combineReducers} from 'redux';
import ResurcesReducer from './resurcesReducer';
import TagViewReducer from './tagViewReducer';

export default combineReducers({
    resurces: ResurcesReducer,
    tagView: TagViewReducer
});