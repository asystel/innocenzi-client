import { GET_TAG_LOADING, GET_TAG_SUCCESS, GET_TAG_ERROR } from '../actions/types';

const INITIAL_STATE = {
    data:null,
    error:null,
    loading:false,
    dataupdate:null
};

export default function(state = INITIAL_STATE,action){
    switch (action.type){
        case GET_TAG_SUCCESS:
            return {...INITIAL_STATE,data:action.payload}                   
        case GET_TAG_ERROR:
            return {...INITIAL_STATE,error:'errore'}        
        case GET_TAG_LOADING :
            return {...INITIAL_STATE,data:state.data,loading:true} 
        default:
            return state
    }
}