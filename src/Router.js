import React from 'react';
import App from './App';
import resurceTagView from './component/view/resurceTagView';
import {BrowserRouter as Router, Route } from 'react-router-dom';
 
export default function Root({store}) {
  return (
    <Router>
      <div>
        <Route exact path="/" component={App} />
        <Route path={"/resurceTagView/:"+store} component={resurceTagView} />
      </div>
    </Router>
  );
}